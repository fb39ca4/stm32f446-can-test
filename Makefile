### CMake
CMAKE_BUILD_TYPE = Debug
BUILD_DIR = ./build
J = 1

CMAKE_GENERATOR = Unix Makefiles
#CMAKE_FLAGS = -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON -DCMAKE_RULE_MESSAGES:BOOL=ON -DCMAKE_VERBOSE_MAKEFILE:BOOL=OFF

.PHONY: modm modm-docs cmake-debug cmake-release build-release build-debug clean upload-debug upload-release gdb help

.DEFAULT_GOAL := help

### Targets

help:
	@echo "Usage: make [target] [J=n]"
	@echo "J=n: Selects the number of CPU cores to use while building, e.g. J=4"
	@echo "targets:"
	@echo "    modm: Generates modm library"
	@echo "    modm-docs: Generates modm documentation"
	@echo "    build-debug: Compiles debug (unoptimized) binary"
	@echo "    build-release: Compiles release (optimzied) binary"
	@echo "    upload-debug: Uses OpenOCD to flash debug binary"
	@echo "    upload-release: Uses OpenOCD to flash release binary"
	@echo "    clean: Cleans binaries, forcing recompilation"
	@echo "Example: \"make upload-release J=8\""
	@echo "uses 8 CPU cores to compile a release binary if necessary, then uploads it to the device."

modm/stamp: project.xml
	@lbuild build
	@touch modm/stamp

modm: modm/stamp

modm/docs/stamp: modm/stamp
	@lbuild build -m ":docs"
	@touch modm/docs/stamp

modm-docs: modm/docs/stamp

$(BUILD_DIR)/cmake-build-debug/stamp: modm/stamp
	@cmake -E make_directory $(BUILD_DIR)/cmake-build-debug
	@cd $(BUILD_DIR)/cmake-build-debug && cmake $(CMAKE_FLAGS) -DCMAKE_BUILD_TYPE=Debug -G "$(CMAKE_GENERATOR)" ../..
	@touch $(BUILD_DIR)/cmake-build-debug/stamp

cmake-debug: $(BUILD_DIR)/cmake-build-debug/stamp

$(BUILD_DIR)/cmake-build-release/stamp: modm/stamp
	@cmake -E make_directory $(BUILD_DIR)/cmake-build-release
	@cd $(BUILD_DIR)/cmake-build-release && cmake $(CMAKE_FLAGS) -DCMAKE_BUILD_TYPE=Release -G "$(CMAKE_GENERATOR)" ../..
	@touch $(BUILD_DIR)/cmake-build-release/stamp

cmake-release: $(BUILD_DIR)/release/stamp

build-release: $(BUILD_DIR)/cmake-build-release/stamp
	@cmake --build $(BUILD_DIR)/cmake-build-release --parallel $J

build-debug: $(BUILD_DIR)/cmake-build-debug/stamp
	@cmake --build $(BUILD_DIR)/cmake-build-debug --parallel $J

clean:
	@cmake --build $(BUILD_DIR)/cmake-build-release --target clean
	@cmake --build $(BUILD_DIR)/cmake-build-debug --target clean

upload-debug: build-debug
	@openocd -f modm/openocd.cfg -c "program_debug"

upload-release: build-release
	@openocd -f modm/openocd.cfg -c "program_release"